﻿#include "FormMain.h"
#include <QApplication>
#include <QFile>
int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    QFile file(QString("%1").arg(":/System/Image/black.css"));
    file.open(QFile::ReadOnly);
    QString css =file.readAll();
    qApp->setStyleSheet(css);

    FormMain w;
    w.show();

    return a.exec();
}
