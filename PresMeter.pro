#-------------------------------------------------
#
# Project created by QtCreator 2021-09-01T09:39:30
#
#-------------------------------------------------

QT       += core gui serialport

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets printsupport

TARGET   = PresMeter
TEMPLATE = app

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

CONFIG += c++11

SOURCES += \
        main.cpp \

include ($$PWD/Form/Form.pri)
include ($$PWD/Public/Public.pri)
include ($$PWD/Dev/Dev.pri)
include ($$PWD/Chart/Chart.pri)

INCLUDEPATH         += $$PWD
INCLUDEPATH         += $$PWD/Public
INCLUDEPATH         += $$PWD/Form
INCLUDEPATH         += $$PWD/Dev
INCLUDEPATH         += $$PWD/Chart

DESTDIR     =  $$PWD/../Bin
DEPENDPATH  += $$PWD/../Bin
UI_DIR      += $$PWD/tmp/ui
MOC_DIR     += $$PWD/tmp/moc
OBJECTS_DIR += $$PWD/tmp/obj
RCC_DIR     += $$PWD/tmp/rcc

RESOURCES += \
    resources.qrc

LIBS += -L$$PWD/ThirdLib/libmodbus-3.1.6/src/win32/ -lmodbus

INCLUDEPATH += $$PWD/ThirdLib/libmodbus-3.1.6/src
DEPENDPATH += $$PWD/ThirdLib/libmodbus-3.1.6/src
