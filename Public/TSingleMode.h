#ifndef TSINGLEMODE_H
#define TSINGLEMODE_H

#include <QMutex>

template<typename T>
class TSingleMode
{
protected:
    TSingleMode(){}
    virtual ~TSingleMode(){}

public:
    static T * getInstance()
    {
        if(m_instance == NULL)
        {
            m_mutex.lock();
            if (m_instance == NULL)
            {
                m_instance = new  T;
            }
            m_mutex.unlock();
        }

        return m_instance;
    }

    //摧毁单例
    static void deleteInstance()
    {
        if(m_instance != NULL)
        {
            delete m_instance;
            m_instance = NULL;
        }
    }

private:
    static T * m_instance;
    static QMutex m_mutex;    //加入互斥锁,保证线程安全(如果存在多线程)
};

//静态成员变量初始化
template<typename T>
T* TSingleMode<T>::m_instance = NULL;

template<typename T>
QMutex TSingleMode<T>::m_mutex;

#endif // TSINGLEMODE_H
