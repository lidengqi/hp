﻿#ifndef OPRATCHART_H
#define OPRATCHART_H

#include <QObject>
#include "axistag.h"
#include "qcustomplot.h"
#include <QDebug>
#include "mytracer.h"


class OpratChart : public QCustomPlot
{
    Q_OBJECT
public:
    explicit OpratChart(QWidget *parent = nullptr,QCustomPlot *pCustomPlot=nullptr);
    ~OpratChart();

    void setZoomMode(bool mode);
    void SetShowTracer(bool isShow);

    void AddGraphs(QString name,QColor color);
    void ChangeColor(int index,QColor color);
    void RemoveGraphs(int devIndex);
    void SetLabelText(int devIndex,double value);
    void SetTrance(QCPGraph *GraphIndex);
    void clearTrance();
    int GetFps();
private  slots:
    virtual void mousePressEvent(QMouseEvent * event);
    virtual void mouseMoveEvent(QMouseEvent * event);
    virtual void mouseReleaseEvent(QMouseEvent * event);

public:
    myTracer * m_TraserD = nullptr;
private:
    QList<QColor> line_colors;
    QList<QColor> gui_colors;
    QList<AxisTag*> m_filmTag;  //图形的tag

    QCustomPlot *Plot;
    //
    bool mZoomMode;
    QRubberBand * mRubberBand;
    QPoint mOrigin;

    int XX[2];

    bool ShowTracer=false;
    QCPItemTracer *tracer;
    QCPItemText *tracerLabel;
    //游标显示
    //myTracer * m_TraserX = nullptr;
    //myTracer * m_TraserY = nullptr;

    QCPGraph * GraphIndexed= nullptr;
    int fps=0;
};

#endif // OPRATCHART_H
