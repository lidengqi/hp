HEADERS += \
    $$PWD/axistag.h \
    $$PWD/mytracer.h \
    $$PWD/opratchart.h \
    $$PWD/qcustomplot.h

SOURCES += \
    $$PWD/axistag.cpp \
    $$PWD/mytracer.cpp \
    $$PWD/opratchart.cpp \
    $$PWD/qcustomplot.cpp

