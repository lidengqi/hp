#ifndef FORMCALIB_H
#define FORMCALIB_H

#include <QWidget>
#include "TSingleMode.h"
#include "qcustomplot.h"
#include "Linearfit.h"

namespace Ui {
class FormCalib;
}
enum eStyle
{
    line,
    lines
};
enum eUnit
{
    N,
    Kgf
};
struct SCalibData
{
    int Source;
    double ActValue;
    SCalibData(int s=0,double a=0.0)
    {
        Source=s;
        ActValue=a;
    }
};

struct SCalibRlt
{
    double K;
    double B;
    SCalibRlt(int k=0.0,double b=0.0)
    {
        K=k;
        B=b;
    }
};

class FormCalib : public QWidget,public TSingleMode<FormCalib>
{
    friend class TSingleMode<FormCalib>;
    Q_OBJECT
public:
    explicit FormCalib(QWidget *parent = nullptr);
    ~FormCalib();
private slots:
    void on_pushButton_AddData_clicked();
    void on_pushButton_DeleteRow_clicked();
    void on_comboBox_Style_activated(int index);
    void on_tableWidget_cellClicked(int row, int column);
    void on_pushButton_Import_clicked();
    void on_pushButton_Export_clicked();
    void on_comboBox_Unit_activated(int index);

private:
    void InitChart();
    void InitListTable();
    void UpdateTable();
    void AddItemContent(int row, int column, QString content);
    void ReadFileCalib(QString file);
    void WriteFileCalib(QString file);
private:
    Ui::FormCalib *ui;
    const int MaxPiont=50;
    int TableSelect=0;

    QList<SCalibData>s_CalibList;
    QList<SCalibRlt>s_CalibRltList;
    //标定文件
    eUnit ClibUnit=N;
    eStyle ClibStyle=line;

};

#endif // FORMCALIB_H
