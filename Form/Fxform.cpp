﻿#include "Fxform.h"
#include "ui_Fxform.h"

FxForm::FxForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FxForm)
{
    ui->setupUi(this);

    //曲线初始化
    InitChart();

    StatusGroup=new QButtonGroup(this);
    StatusGroup->addButton(ui->rB_All,0);
    StatusGroup->addButton(ui->rB_Rect,1);
    StatusGroup->addButton(ui->rB_Rect_Cnt,2);

    ui->rB_All->setChecked(true);
    connect(StatusGroup, QOverload<int>::of(&QButtonGroup::buttonClicked),[=](int id)
    {
        if(id==0)
        {
            ui->widget_Chart->setInteraction(QCP::iRangeDrag,true);//可拖动
            ui->widget_Chart->setSelectionRectMode(QCP::SelectionRectMode::srmNone);
            ui->widget_Chart->graph(0)->setSelectable(QCP::SelectionType::stWhole);
        }
        else if(id==1)
        {
            ui->widget_Chart->setInteraction(QCP::iRangeDrag,false);//取消拖动
            ui->widget_Chart->setSelectionRectMode(QCP::SelectionRectMode::srmSelect);
            ui->widget_Chart->graph(0)->setSelectable(QCP::SelectionType::stMultipleDataRanges);
        }
        else if(id==2)
        {
            ui->widget_Chart->setInteraction(QCP::iRangeDrag,false);//取消拖动
            ui->widget_Chart->setSelectionRectMode(QCP::SelectionRectMode::srmSelect);
            ui->widget_Chart->graph(0)->setSelectable(QCP::SelectionType::stDataRange);
        }
    });

    //从文件内加载面数据
    connect(ui->Btn_LoadFile,&QCheckBox::clicked,[=]()
    {
        QStringList listFile = QFileDialog::getOpenFileNames(this, "请选择保存项目文件路径", "", "*.csv");
        if(listFile.count()==0)
            return;
        GetFpsData(listFile,RCList);

        ui->label_Pts->setText(QString("%1").arg(RCList.count()));

        for (int var = 0; var < RCList.count(); ++var) {
            //添加到曲线控件
            ui->widget_Chart->graph(0)->addData((double)var,RCList.at(var).data);
            pCustomPlot->SetLabelText(0,RCList.at(var).data);
        }
        ui->widget_Chart->xAxis->rescale(true);
        ui->widget_Chart->yAxis->rescale(true);
        ui->widget_Chart->replot(QCustomPlot::rpQueuedReplot);

    });

    QTimer *RunTimer=new QTimer();
    connect(RunTimer,SIGNAL(timeout()),this,SLOT(TimeUpdate()));
    RunTimer->start(100);
}

FxForm::~FxForm()
{
    delete ui;
}

void FxForm::InitChart()
{
    pCustomPlot=new OpratChart(this,ui->widget_Chart);
    pCustomPlot->AddGraphs("F_RC",QColor("#00FB00"));
    ui->widget_Chart->graph(0)->setScatterStyle(QCPScatterStyle(QCPScatterStyle::ssCircle, 5));
    pCustomPlot->SetTrance(ui->widget_Chart->graph(0));
    pCustomPlot->SetShowTracer(true);
}

void FxForm::TimeUpdate()
{
    if(0<RCList.count()&&0<=pCustomPlot->GetFps()&&pCustomPlot->GetFps()<RCList.count())
    {
        ui->label_RcTime->setText(RCList.at(pCustomPlot->GetFps()).time);
    }
}

void FxForm::on_widget_Chart_selectionChangedByUser()
{
    QCustomPlot *pCustomPlot=ui->widget_Chart;
    QVector<double> SelectValue;
    qint64 isl=0;
    if(pCustomPlot->graph(0)->selected())
    {
        QCPDataSelection selection =pCustomPlot->graph(0)->selection();
        SelectValue.clear();
        //遍历选中范围
        for(int j=0;j<selection.dataRangeCount();j++)
        {
            QCPDataRange dataRange = selection.dataRange(j);
            //遍历数据
            for(int k=dataRange.begin();k<dataRange.end();k++)
            {
                QString str_key = QString::number(pCustomPlot->graph(0)->data()->at(k)->key);
                QString str_value = QString::number(pCustomPlot->graph(0)->data()->at(k)->value);
                QString str_at= pCustomPlot->graph(0)->name();
                SelectValue.append(pCustomPlot->graph(0)->data()->at(k)->value);
                // QListWidgetItem *Items=new QListWidgetItem();
                // Items->setText(""+str_at+"："+str_key+", "+str_value);
                // Items->setTextColor(pCustomPlot->graph(i)->pen().color());
                // ui->listWidget->addItem(Items);
                isl++;
            }
        }
        //计算最大值，最小值，平均值
        auto max=std::max_element(std::begin(SelectValue),std::end(SelectValue));
        auto min=std::min_element(std::begin(SelectValue),std::end(SelectValue));
        double biggest = *max;
        double smallest = *min;
        double Avrage = std::accumulate(std::begin(SelectValue), std::end(SelectValue),0.000)/SelectValue.count();

        ui->label_Pts_sl->setText(QString("%1").arg(isl));
        ui->label_Max->setText(QString("%1").arg(biggest));
        ui->label_Avg->setText(QString("%1").arg(Avrage));
        ui->label_Min->setText(QString("%1").arg(smallest));
    }

}
bool FxForm::GetFpsData(QStringList listFile,QList<SRCDATA> &RCList)
{
    RCList.clear();
    for (int index = 0; index < listFile.count(); ++index)
    {
        QString strFile=listFile.at(index);
        QFile file(strFile);

        if (!file.open(QIODevice::ReadOnly))
            return false;

        QProgressDialog process;
        process.setWindowTitle("Import data");
        process.setLabelText(listFile.at(index));
        process.setRange(0,file.size());
        qDebug()<<file.size();
        process.setCancelButtonText(tr("Cancel"));
        process.showNormal();
        qint64 iProcess=0;
        QTextStream Stream(&file);
        QStringList listdata;
        SRCDATA  FpsData;
        while(!Stream.atEnd())
        {
            QCoreApplication::processEvents();
            QString line = Stream.readLine();
            iProcess+=line.size();
            process.setValue(iProcess);
            if(process.wasCanceled())
            {
                return false;
            }
            listdata=line.split(',');
            FpsData.time=listdata.at(0);
            FpsData.data=QString(listdata.at(1)).toFloat();
            RCList.append(FpsData);
        }
        file.close();
    }
    return true;
}
