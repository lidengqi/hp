﻿#ifndef FXFORM_H
#define FXFORM_H

#include <QWidget>
#include "TSingleMode.h"
#include "OpratChart.h"

namespace Ui {
class FxForm;
}

struct SRCDATA
{
    QString time;
    float data;
};

class FxForm : public QWidget,public TSingleMode<FxForm>
{
    friend class TSingleMode<FxForm>;
    Q_OBJECT

public:
    explicit FxForm(QWidget *parent = nullptr);
    ~FxForm();
    bool GetFpsData(QStringList listFile,QList<SRCDATA> &RCList);
private:
    void InitChart();
private:
    Ui::FxForm *ui;
    OpratChart  *pCustomPlot=nullptr;
    QList<SRCDATA> RCList;
    QButtonGroup *StatusGroup;
private slots:
    void TimeUpdate();
    void on_widget_Chart_selectionChangedByUser();
};

#endif // FXFORM_H
