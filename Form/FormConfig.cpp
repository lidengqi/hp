﻿#include "FormConfig.h"
#include "ui_FormConfig.h"
#include <QSerialPortInfo>
#include <QMessageBox>
#include <QDir>
#include <QTextCodec>

FormConfig::FormConfig(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormConfig)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);
    setWindowTitle("端口配置");
    setWindowIcon(QIcon(":/Other/Image/Port1.png"));

    p_HP=new AcqPressure;
    QStringList m_serialPortName;
    foreach(const QSerialPortInfo &info,QSerialPortInfo::availablePorts())
    {
        m_serialPortName << info.portName();
    }
    ui->Cbx_Port->clear();
    ui->Cbx_Port->addItems(m_serialPortName);

    this->ReadSerial();

    ui->Cbx_Port->setCurrentText(p_HP->s_HP.Port);
    ui->Cbx_Bts->setCurrentText(QString::number(p_HP->s_HP.BaudRate));
    ui->Cbx_Bits->setCurrentText(QString::number(p_HP->s_HP.DataBits));
    ui->Cbx_StopBits->setCurrentText(QString::number(p_HP->s_HP.StopBits));
    ui->Cbx_check->setCurrentText(QString::number(p_HP->s_HP.Check));


    connect(ui->Btn_Connect,&QPushButton::clicked,[=](){

        p_HP->s_HP.Port=ui->Cbx_Port->currentText();
        p_HP->s_HP.BaudRate=ui->Cbx_Bts->currentText().toInt();
        p_HP->s_HP.DataBits=ui->Cbx_Bits->currentText().toInt();
        p_HP->s_HP.StopBits=ui->Cbx_StopBits->currentText().toInt();
        p_HP->s_HP.Check=ui->Cbx_check->currentText().toInt();

        if(!p_HP->Connect())
        {
            if(p_HP->DisConnect())
            {
                ui->Btn_Connect->setText("连接");
                return;
            }
        }
        int delay=0;
        while(!p_HP->isConnect)
        {
            p_HP->delaymsec(100);
            delay++;
            if(delay>10)
            {
                QMessageBox::warning(this,"提示","连接超时");
                ui->Btn_Connect->setText("连接");
                return;
            }
        }
        ui->Btn_Connect->setText("已连接");
        this->WriteSerial();
        this->close();
    });
}

FormConfig::~FormConfig()
{
    delete ui;
}

void FormConfig::WriteSerial()
{
    QSettings mSettings(QDir::currentPath()+"/Settings.ini",QSettings::IniFormat);
    mSettings.setIniCodec(QTextCodec::codecForName("UTF-8"));
    mSettings.setValue("Serial/Port",p_HP->s_HP.Port);
    mSettings.setValue("Serial/BaudRate",p_HP->s_HP.BaudRate);
    mSettings.setValue("Serial/DataBits",p_HP->s_HP.DataBits);
    mSettings.setValue("Serial/StopBits",p_HP->s_HP.StopBits);
    mSettings.setValue("Serial/Check",p_HP->s_HP.Check);
}

void FormConfig::ReadSerial()
{
    QSettings mSettings(QDir::currentPath()+"/Settings.ini",QSettings::IniFormat);
    mSettings.setIniCodec(QTextCodec::codecForName("UTF-8"));
    p_HP->s_HP.Port=mSettings.value("Serial/Port").toString();
    p_HP->s_HP.BaudRate=mSettings.value("Serial/BaudRate").toInt();
    p_HP->s_HP.DataBits=mSettings.value("Serial/DataBits").toInt();
    p_HP->s_HP.StopBits=mSettings.value("Serial/StopBits").toInt();
    p_HP->s_HP.Check=mSettings.value("Serial/Check").toInt();
}
