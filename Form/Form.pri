FORMS += \
    $$PWD/FormCalib.ui \
    $$PWD/FormHpConfig.ui \
    $$PWD/FormMain.ui\
    $$PWD/FormConfig.ui \
    $$PWD/Fxform.ui

HEADERS += \
    $$PWD/FormCalib.h \
    $$PWD/FormHpConfig.h \
    $$PWD/FormMain.h\
    $$PWD/FormConfig.h \
    $$PWD/Fxform.h

SOURCES += \
    $$PWD/FormCalib.cpp \
    $$PWD/FormHpConfig.cpp \
    $$PWD/FormMain.cpp\
    $$PWD/FormConfig.cpp \
    $$PWD/Fxform.cpp

