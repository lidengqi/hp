﻿#ifndef FORMCONFIG_H
#define FORMCONFIG_H

#include <QWidget>
#include <QSettings>
#include "TSingleMode.h"
#include "HP_Modbus.h"

namespace Ui {
class FormConfig;
}

class FormConfig : public QWidget,public TSingleMode<FormConfig>
{
    friend class TSingleMode<FormConfig>;
    Q_OBJECT

public:
    explicit FormConfig(QWidget *parent = nullptr);
    ~FormConfig();

public:
    AcqPressure *p_HP=nullptr;
    struct Measure
    {
        QString  Unit="";
        uchar Accuracy=2;
        uchar Style=2;
        uint  Fre=30;
        QString SavePath="";
    };
private:
    Ui::FormConfig *ui;

public:
    void WriteSerial();
    void ReadSerial();
signals:
    void SendConnectState(bool state);
};

#endif // FORMCONFIG_H
