﻿#include "FormHpConfig.h"
#include "ui_FormHpConfig.h"
#include <QIcon>
#include "FormConfig.h"

FormHpConfig::FormHpConfig(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormHpConfig)
{
    ui->setupUi(this);
    setWindowFlags(Qt::Window | Qt::WindowTitleHint | Qt::CustomizeWindowHint | Qt::WindowCloseButtonHint);
    setWindowTitle("参数配置");
    setWindowIcon(QIcon(":/Other/Image/Config.png"));

    connect(ui->dSb_Up, QOverload<double>::of(&QDoubleSpinBox::valueChanged),[=](double d){

    });
}

FormHpConfig::~FormHpConfig()
{
    delete ui;
}
