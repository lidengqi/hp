﻿#ifndef FORMMAIN_H
#define FORMMAIN_H

#include <QWidget>
#include "opratchart.h"
#include "Fxform.h"


namespace Ui {
class FormMain;
}

class FormMain : public QWidget
{
    Q_OBJECT

public:
    explicit FormMain(QWidget *parent = nullptr);
    ~FormMain();
protected:
    void closeEvent(QCloseEvent *event);

private:
    Ui::FormMain *ui;
    OpratChart  *pCustomPlot=nullptr;
    QString RecordFileName;
    QStringList RangList;
    QStringList UnitList;
    qint64 PtCount=0;

    //记录
    QString RcFileName="";
    QFile RcfileSave;
    QTextStream RcStream;
    int RcFileCount=0;
    QList<SRCDATA> RCList;
private:
    void InitChart();
    void AutoRcNameSimple();
    void ClearChart();

private slots:
    void RecvAcqpre();
    void TimeUpdate();
};

#endif // FORMMAIN_H
