﻿#ifndef FORMHPCONFIG_H
#define FORMHPCONFIG_H

#include <QWidget>
#include <QSettings>
#include "TSingleMode.h"

namespace Ui {
class FormHpConfig;
}

class FormHpConfig : public QWidget,public TSingleMode<FormHpConfig>
{
    friend class TSingleMode<FormHpConfig>;
    Q_OBJECT

public:
    explicit FormHpConfig(QWidget *parent = nullptr);
    ~FormHpConfig();

private:
    Ui::FormHpConfig *ui;


};

#endif // FORMHPCONFIG_H
