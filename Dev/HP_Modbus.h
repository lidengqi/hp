﻿/***************************************************************************
**           Author: Lidengqi                                             **
**          WeChart: frank00h                                             **
**             Date: 01.09.21                                             **
**          Version: 1.0.0                                                **
****************************************************************************/

#ifndef ACQPRESSURE_H
#define ACQPRESSURE_H

#include <QThread>
#include <QSerialPort>
#include <QTime>
#include <QCoreApplication>
#include <QObject>
#include "modbus-rtu.h"


enum eDataAddr
{
    Value=3,   //主读数
    HigLmt=7,       //上限设定
    LowLmt=11,       //下限设定
    CompValue=15,    //比较值设定
    Acc=19, //重力加速度
    Range=23,          //量程
    Unit=25,           //单位
    Record=27          //记录数
};

class AcqPressure:public QThread
{
    Q_OBJECT
public:
    AcqPressure();
    ~AcqPressure();
    bool Connect();      //连接设备
    bool DisConnect();   //断开设备
    void SetParam(QByteArray buff);
    void delaymsec(int msec);
public:
    struct HP
    {
        QString Port;
        int BaudRate=9600;
        int StopBits=1;
        int DataBits=8;
        char Parity='N';
        int Check=0;
    }s_HP;

    struct HPData
    {
        float Value;   //主读数
        float Up;      //上限设定
        float Down;    //下限设定
        float Compare; //比较值设定
        float Acc; //重力加速度
        uint Range;  //量程
        uint unit;   //单位
        uint Rcords; //记录数
    } s_HpData;

    bool isConnect=false;    //设备是否连接
    bool isStart=false;


    uint Fre=10;
private:
    bool AutoOpen(modbus_t *modbus);
    void setParam(modbus_t *modbus);
    void run();

private:
    QByteArray  recvbuff;
    bool isSetParam=false;
    QByteArray SendParambuff;
    bool isBreakThread=false;       //退出线程
    //libmodbus
    modbus_t *p_modbus=nullptr;
    uint16_t inputData[8];
signals:
    void SigAcqpre();  //数据实时处理

};
#endif // ACQPRESSURE_H
