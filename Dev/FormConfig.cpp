#include "FormConfig.h"
#include "ui_FormConfig.h"

FormConfig::FormConfig(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::FormConfig)
{
    ui->setupUi(this);
}

FormConfig::~FormConfig()
{
    delete ui;
}
